package org.sang.controller.client;

import org.sang.bean.RespBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("client")
public class IndexController {
    @RequestMapping("/index")
    public RespBean getIndex () {
        return new RespBean("success", "这里是首页");
    }
}
